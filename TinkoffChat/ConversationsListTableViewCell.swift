//
//  ConversationsListTableViewCell.swift
//  TinkoffChat
//
//  Created by Igor Hromov on 25.03.17.
//  Copyright © 2017 TCS. All rights reserved.
//

import UIKit

protocol ConversationCellConfiguration : class {
    var name : String? {get set}
    var message : String? {get set}
    var date : Date? {get set}
    var online : Bool {get set}
    var hasUnreadMessages : Bool {get set}
}

class ConversationsListTableViewCell: UITableViewCell, ConversationCellConfiguration {
    
    @IBOutlet weak var timeCellLabel: UILabel!
    @IBOutlet weak var nameCellLabel: UILabel!
    @IBOutlet weak var messagePreviewCellLabel: UILabel!
    
    var name : String?
    var message : String?
    var date : Date?
    var online : Bool = false
    var hasUnreadMessages : Bool = false
    
    let historyColor = UIColor.white
    let onlineColor = UIColor(red: 1, green: 1, blue: 224/255, alpha: 0.5)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    
    
    
}
