//
//  messageCell.swift
//  TinkoffChat
//
//  Created by Igor Hromov on 29.03.17.
//  Copyright © 2017 TCS. All rights reserved.
//

import UIKit

protocol MessageCellConfiguration : class {
    var messageText : String? {get set}
}

class MessageCell: UITableViewCell, MessageCellConfiguration {
    
    @IBOutlet weak var textMessageCell: UILabel!
    
    var messageText : String?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
