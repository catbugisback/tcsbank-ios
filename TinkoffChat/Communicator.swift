//
//  Communicator.swift
//  TinkoffChat
//
//  Created by Igor Hromov on 10.04.17.
//  Copyright © 2017 TCS. All rights reserved.
//

import UIKit

protocol Communicator {
    func sendMessage(string: String, to userID: String, completitionHandler: ((_ seccess : Bool, _ error: Error?) -> ())?)
    weak var delegate : CommunicatorDelegate? {get set}
    var online : Bool {get set}
}
