//
//  GCDDataManager.swift
//  TinkoffChat
//
//  Created by Igor Hromov on 31.03.17.
//  Copyright © 2017 TCS. All rights reserved.
//

import UIKit

class GCDDataManager : SaveManager {
    
    func saveProfile(viewController: ProfileViewController) {
        userInitiatedQueue.async {
            sleep(3000)
            self.saveUserImage(image: viewController.profileImage.image!)
            self.saveTextToFile(text: viewController.loginTextField.text!, fileName: "profile_name.txt")
            self.saveTextToFile(text: viewController.descriptionTextView.text, fileName: "profile_description.txt")
        }
    }
    
private
    
    let userInitiatedQueue = DispatchQueue.global(qos : .userInitiated)
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func saveUserImage(image: UIImage) {
        if let data = UIImagePNGRepresentation(image) {
            let path = getDocumentsDirectory().appendingPathComponent("profile.png")
            try? data.write(to: path)
            print("Saved")
        }
    }
    
    func saveTextToFile(text : String, fileName : String) {
        let path = getDocumentsDirectory().appendingPathComponent(fileName)
        do {
            try text.write(to: path, atomically: false, encoding: String.Encoding.utf8)
        } catch {
            
        }
    }
}
