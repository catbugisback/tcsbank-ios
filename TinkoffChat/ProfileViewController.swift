//
//  ViewController.swift
//  TinkoffChat
//
//  Created by Igor Hromov on 04.03.17.
//  Copyright © 2017 TCS. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var savingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var saveOperationButton: UIButton!
    @IBOutlet weak var saveGCDButton: UIButton!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.loginTextField.delegate = self;
        self.loginTextField.returnKeyType = UIReturnKeyType.done
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapAction))
        view.addGestureRecognizer(tapGesture)
        savingIndicator.hidesWhenStopped = true
    }
    
    
    func onTapAction() {
        view.endEditing(true)
    }
    
    func savingPreperation() {
        savingIndicator.startAnimating()
        saveOperationButton.isUserInteractionEnabled = false
        saveGCDButton.isUserInteractionEnabled = false
    }
    
    
    func savingDone() {
        savingIndicator.stopAnimating()
        saveOperationButton.isUserInteractionEnabled = true
        saveGCDButton.isUserInteractionEnabled = true
    }

    @IBAction func saveWithGCD(_ sender: UIButton) {
        savingPreperation()
        let saveGCDObject = GCDDataManager()
        saveGCDObject.saveProfile(viewController: self)
        savingDone()
        let alert = UIAlertController(title: "Данные сохранены", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ОК", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)

        
    }
    
    @IBAction func saveWithOperation(_ sender: Any) {
        savingPreperation()
        
        savingDone()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func changeButtonColor(_ sender: UIButton) {
        self.colorLabel.textColor = sender.backgroundColor
    }
    @IBAction func dismissViewController(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
