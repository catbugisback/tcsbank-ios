//
//  Comunicatordelegate.swift
//  TinkoffChat
//
//  Created by Igor Hromov on 10.04.17.
//  Copyright © 2017 TCS. All rights reserved.
//

import UIKit

protocol CommunicatorDelegate : class {
    //discovering 
    func didFoundUser(userID : String, userName : String?)
    func didLostUser(userID : String)
    
    //errors
    func failedToStartBrowsingForUsers(error : Error)
    func failedToStartAdvertising(error : Error)
    
    //messages
    func didRecieveMessage(text : String, fromUser : String, toUser : String)
}
