//
//  CommunicationMenager.swift
//  TinkoffChat
//
//  Created by Igor Hromov on 11.04.17.
//  Copyright © 2017 TCS. All rights reserved.
//

import UIKit

class CommunicationManager : CommunicatorDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var tableDelegate : UITableView!
    
    
    func didLostUser(userID: String) {
        for (index, aPeer) in  appDelegate.mpcManager.foundUsers.enumerated() {
            if aPeer == userID {
                appDelegate.mpcManager.foundUsers.remove(at: index)
                break
            }
        }
        if let delegateT = tableDelegate {
            delegateT.reloadData()
        }
    }
    
    func didFoundUser(userID: String, userName: String?) {
        appDelegate.mpcManager.foundUsers.append(userID)
        if let delegateT = tableDelegate {
            delegateT.reloadData()
        }
    }
    
    func didRecieveMessage(text: String, fromUser: String, toUser: String) {
        
    }
    
    func failedToStartAdvertising(error: Error) {
        
    }
    
    func failedToStartBrowsingForUsers(error: Error) {
        
    }
}
