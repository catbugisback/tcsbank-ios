//
//  ProtocolSaveManager.swift
//  TinkoffChat
//
//  Created by Igor Hromov on 02.04.17.
//  Copyright © 2017 TCS. All rights reserved.
//

import UIKit

protocol SaveManager {
    func saveProfile(viewController : ProfileViewController)
}
